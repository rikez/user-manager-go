CREATE TABLE IF NOT EXISTS users (
    id               BINARY(16) NOT NULL,
    email            VARCHAR(200) NOT NULL,
    full_name        VARCHAR(100),
    `address`        TEXT,
    telephone        VARCHAR(50),
    password_hash    VARCHAR(255),
    third_party_auth VARCHAR(30),
	created_at       TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP(3) NOT NULL,
    updated_at       TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),

    PRIMARY KEY (id),
    UNIQUE KEY idx_unique_email (email)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;