package oauth2provider

import (
	"encoding/base64"
	"fmt"

	uuid "github.com/satori/go.uuid"
)

// State represents the security parameter in OAuth2
type State struct {
	val []byte
}

func newState() State {
	newUUID := uuid.NewV1()
	return State{val: newUUID.Bytes()}
}

func (o State) String() string {
	return string(o.val)
}

// Encode encodes the state to Base64
func (o State) Encode() string {
	return base64.StdEncoding.EncodeToString(o.val)
}

// DecodeState decodes the incoming state from base64 to State
func DecodeState(state string) (State, error) {
	b, err := base64.StdEncoding.DecodeString(state)
	if err != nil {
		return State{}, fmt.Errorf("decode oauth2 state failed: %v", err)
	}

	return State{val: b}, nil
}
