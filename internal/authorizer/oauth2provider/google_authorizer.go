package oauth2provider

import (
	"context"
	"fmt"

	"gitlab.com/rikez/user-manager-go.git/internal/customerrors"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type googleAuthorizer struct {
	oauthCfg oauth2.Config
}

// NewGoogleAuthorizer creates a new Google OAuth2 authorizer
func NewGoogleAuthorizer(cfg Config) Authorizer {
	return &googleAuthorizer{oauthCfg: newOAuth2Config(cfg)}
}

func newOAuth2Config(cfg Config) oauth2.Config {
	return oauth2.Config{
		ClientID:     cfg.ClientID,
		ClientSecret: cfg.ClientSecret,
		RedirectURL:  cfg.RedirectURL,
		Scopes:       scopes,
		Endpoint:     google.Endpoint,
	}
}

func (s *googleAuthorizer) Type() string {
	return GoogleAuthorizerType.String()
}

func (s *googleAuthorizer) GetRedirectConfig() *SignInRedirectConfig {
	return &SignInRedirectConfig{
		Config: s.oauthCfg,
		State:  newState(),
	}
}

func (s *googleAuthorizer) Exchange(ctx context.Context, code string) (*oauth2.Token, error) {
	token, err := s.oauthCfg.Exchange(ctx, code)
	if err != nil {
		return nil, customerrors.NewErrInternal(fmt.Errorf("exchange code failed: %s", err))
	}
	return token, nil
}
