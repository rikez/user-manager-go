package rest

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/user-manager-go.git/internal/usecase/user"
)

// Server acts as a web server resposible for rendering, auth, etc
type Server struct {
	userService user.Usecases
}

// NewServer creates anew instance of server
func NewServer(userService user.Usecases) *Server {
	return &Server{userService}
}

// AddRoutes defines routers' handlers
func (s *Server) AddRoutes(r *mux.Router) {
	r.HandleFunc("/v1/{id}", s.wrappedHandler(s.getUser)).Methods("GET")
	r.HandleFunc("/v1/{id}", s.wrappedHandler(s.updateUser)).Methods("PUT")
	r.HandleFunc("/v1/google", s.wrappedHandler(s.createUserFromGoogleAccount)).Methods("POST")
	r.HandleFunc("/v1", s.wrappedHandler(s.createUser)).Methods("POST")
	r.HandleFunc("/v1/login", s.wrappedHandler(s.loginUser)).Methods("POST")
	r.HandleFunc("/v1/reset_password", s.wrappedHandler(s.resetPassword)).Methods("POST")
	r.HandleFunc("/v1/reset_password/verify", s.wrappedHandler(s.verifyResetPasswordToken)).Methods("POST")
}

func (s *Server) getUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userID, err := s.validateUserID(r)
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	usr, err := s.userService.GetByID(ctx, userID)
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, usr)
}

func (s *Server) updateUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userID, err := s.validateUserID(r)
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	var usr UpdateUserRequest
	if err := s.validateStruct(r, &usr); err != nil {
		handleError(ctx, w, err)
		return
	}

	updatedUsr, err := s.userService.Update(ctx, usr.mapUpdateRequest(userID))
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, updatedUsr)
}

func (s *Server) createUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var usr CreateOrLoginUserRequest
	if err := s.validateStruct(r, &usr); err != nil {
		logrus.Error(err)
		handleError(ctx, w, err)
		return
	}

	createdUsr, err := s.userService.Register(ctx, usr.mapCreateOrLoginRequest())
	if err != nil {
		logrus.Error(err)
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusCreated, createdUsr)
}

func (s *Server) createUserFromGoogleAccount(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var payload GoogleUserRequest
	if err := s.validateStruct(r, &payload); err != nil {
		handleError(ctx, w, err)
		return
	}

	updatedUsr, err := s.userService.RegisterFromGoogle(ctx, payload.AccessToken)
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusCreated, updatedUsr)
}

func (s *Server) loginUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var usr CreateOrLoginUserRequest
	if err := s.validateStruct(r, &usr); err != nil {
		handleError(ctx, w, err)
		return
	}

	result, err := s.userService.SignIn(ctx, usr.Email, usr.Password)
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, result)
}

func (s *Server) resetPassword(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var usr ResetPasswordRequest
	if err := s.validateStruct(r, &usr); err != nil {
		handleError(ctx, w, err)
		return
	}

	if err := s.userService.ResetPassword(ctx, usr.Email); err != nil {
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"status": "ok"})
}

func (s *Server) verifyResetPasswordToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var payload VerifyResetPasswordTokenRequest
	if err := s.validateStruct(r, &payload); err != nil {
		handleError(ctx, w, err)
		return
	}

	usr, err := s.userService.VerifyResetPasswordToken(ctx, payload.Token)
	if err != nil {
		handleError(ctx, w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, usr)
}
