package rest

import (
	"gitlab.com/rikez/user-manager-go.git/internal/model"
)

// CreateOrLoginUserRequest represents the request body when creating or login an user
type CreateOrLoginUserRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=8,max=50"`
}

func (c CreateOrLoginUserRequest) mapCreateOrLoginRequest() model.User {
	return model.User{
		Email:    c.Email,
		Password: c.Password,
	}
}

// GoogleUserRequest represents the request body when creating/login an user from their google account info
type GoogleUserRequest struct {
	AccessToken string `json:"access_token" validate:"required"`
}

// UpdateUserRequest represents the request body when updating an user
type UpdateUserRequest struct {
	FullName  string `json:"full_name" validate:"omitempty,min=3,max=100"`
	Telephone string `json:"telephone" validate:"omitempty,min=5,max=50"`
	Address   string `json:"address" validate:"omitempty,min=5,max=255"`
	Email     string `json:"email" validate:"omitempty,email"`
	Password  string `json:"password,omitempty" validate:"omitempty,min=8,max=50"`
}

func (u UpdateUserRequest) mapUpdateRequest(id string) model.User {
	return model.User{
		ID:        id,
		FullName:  &u.FullName,
		Telephone: &u.Telephone,
		Address:   &u.Address,
		Email:     u.Email,
		Password:  u.Password,
	}
}

// ResetPasswordRequest represents the request body when resetting a password
type ResetPasswordRequest struct {
	Email string `json:"email" validate:"required,email"`
}

// VerifyResetPasswordTokenRequest represents the request body when verifying a reset password token
type VerifyResetPasswordTokenRequest struct {
	Token string `json:"token" validate:"required"`
}
