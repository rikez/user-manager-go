package rest

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/user-manager-go.git/internal/customerrors"
	"gitlab.com/rikez/user-manager-go.git/internal/generator"
)

var structValidator *validator.Validate

func init() {
	structValidator = validator.New()
}

func (s *Server) wrappedHandler(h func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	var handler http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
		t0 := time.Now()
		reqID := generator.NewUUID().String()
		fields := logrus.Fields{"reqid": reqID}

		logrus.WithFields(fields).Infof("Request: %s %s", r.Method, r.URL.Path)

		// TODO: find a way to get status code
		h(w, r)
		elapsedMs := float64(time.Now().UnixNano()-t0.UnixNano()) / float64(time.Millisecond)

		logrus.WithFields(fields).Infof("Response: %s %s -> (%.2f ms)", r.Method, r.URL.Path, elapsedMs)
	}

	return handler
}

// generic error handling for the http requests
func handleError(ctx context.Context, w http.ResponseWriter, err error) {
	ctxLogger := logrus.WithContext(ctx)

	ctxLogger.Infof("Replied with error: %T - %v", err, err)

	var statusCode int

	var logFn func(format string, args ...interface{})

	switch err.(type) {
	case customerrors.ErrMalformed:
		logFn = ctxLogger.Errorf
		statusCode = http.StatusBadRequest
	case customerrors.ErrResourceNotFound:
		logFn = ctxLogger.Infof
		statusCode = http.StatusNotFound
	case customerrors.ErrForbidden:
		logFn = ctxLogger.Warningf
		statusCode = http.StatusForbidden
	case customerrors.ErrConflict:
		logFn = ctxLogger.Warningf
		statusCode = http.StatusConflict
	default:
		logFn = ctxLogger.Errorf
		statusCode = http.StatusInternalServerError
	}

	logFn("Replying status code %d. Error: %s", statusCode, err.Error())

	respondWithError(w, statusCode, err.Error())
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]interface{}{"error": message, "status": code})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")

	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte(`{ "error": "Internal Server Error", "status": "500"}`))
		if err != nil {
			panic(err)
		}
	}

	w.WriteHeader(code)
	_, err = w.Write(response)
	if err != nil {
		panic(err)
	}
}

func (s *Server) validateUserID(r *http.Request) (string, error) {
	vars := mux.Vars(r)

	userID, err := generator.ParseUUID(vars["id"])
	if err != nil {
		return "", customerrors.NewErrMalformed(errors.New("invalid uuid"))
	}

	return userID.String(), nil
}

func (s *Server) validateStruct(r *http.Request, v interface{}) error {
	if err := json.NewDecoder(r.Body).Decode(&v); err != nil {
		return customerrors.NewErrMalformed(err)
	}
	defer r.Body.Close()

	if err := structValidator.Struct(v); err != nil {
		return customerrors.NewErrMalformed(err)
	}
	return nil
}
