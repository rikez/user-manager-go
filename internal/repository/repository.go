package repository

import (
	"context"

	"gitlab.com/rikez/user-manager-go.git/internal/model"
)

//go:generate mockery -case underscore -output mocks -outpkg repositorymock -name Repository

// Repository defines a contract to access storages
type Repository interface {
	// SaveUser inserts a new user into the database
	SaveUser(context.Context, model.User) error
	// FindUserByID returns an user by id
	FindUserByID(ctx context.Context, id string) (*model.User, error)
	// FindUserByEmail returns an user by email
	FindUserByEmail(ctx context.Context, email string) (*model.User, error)
	// UpdateUser updates an user
	UpdateUser(context.Context, model.User) error
}
