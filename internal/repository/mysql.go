package repository

import (
	"context"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/user-manager-go.git/internal/generator"
	"gitlab.com/rikez/user-manager-go.git/internal/model"
)

type defaultRepository struct {
	db *sqlx.DB
}

// NewMySQLRepository creates a new instance of Repository
func NewMySQLRepository(db *sqlx.DB) Repository {
	return &defaultRepository{db: db}
}

func (r *defaultRepository) SaveUser(ctx context.Context, usr model.User) error {
	res, err := sq.Insert("users").
		Columns(sqlInsertColumns...).
		Values(
			generator.Bytes(usr.ID),
			usr.FullName,
			usr.Email,
			usr.PasswordHash,
			usr.ThirdPartyAuth,
		).
		RunWith(r.db).
		ExecContext(ctx)

	if err != nil {
		if IsDuplicateEntryErr(err) {
			return ErrAlreadyExists
		}
		logrus.Errorf("failed to insert new user: %v", err)
		return ErrDatabaseServer
	}

	affected, err := res.RowsAffected()
	if err != nil {
		logrus.Errorf("Failed to get rows affected on SaveUser: %v", err)
		return ErrDatabaseServer
	}
	if affected != 1 {
		logrus.Errorf("Unexpected behaviour with number of rows affected %d on SaveUser", affected)
		return ErrDatabaseServer
	}

	return nil
}

func (r *defaultRepository) FindUserByID(ctx context.Context, id string) (*model.User, error) {
	return r.findOneUserBy(ctx, r.db, map[string]interface{}{"id": generator.Bytes(id)})
}

func (r *defaultRepository) FindUserByEmail(ctx context.Context, email string) (*model.User, error) {
	return r.findOneUserBy(ctx, r.db, map[string]interface{}{"email": email})
}

func (r *defaultRepository) findOneUserBy(ctx context.Context, runner sq.BaseRunner, terms map[string]interface{}) (*model.User, error) {
	builder := sq.Select(sqlQueryColumns...).From("users")

	for k, v := range terms {
		builder = builder.Where(fmt.Sprintf("%s = ?", k), v)
	}

	row := builder.RunWith(runner).QueryRowContext(ctx)

	var dbUsr dbUser
	if err := row.Scan(
		&dbUsr.ID,
		&dbUsr.FullName,
		&dbUsr.Telephone,
		&dbUsr.Address,
		&dbUsr.Email,
		&dbUsr.PasswordHash,
		&dbUsr.ThirdPartyAuth,
		&dbUsr.CreatedAt,
		&dbUsr.UpdatedAt,
	); err != nil {
		if NoRows(err) {
			return nil, ErrNotFound
		}
		logrus.Warnf("Failed to find user by email: %s", err)
		return nil, ErrDatabaseServer
	}

	return dbUsr.ToUser(), nil
}

func (r *defaultRepository) UpdateUser(ctx context.Context, updateUsr model.User) error {
	return NewSQLTransaction(r.db).Do(ctx, func(tx *sqlx.Tx) error {
		usrBinID := generator.Bytes(updateUsr.ID)

		usr, err := r.findOneUserBy(ctx, tx, map[string]interface{}{"id": usrBinID})
		if err != nil {
			return err
		}

		clauses := map[string]interface{}{}

		if usr.ShouldUpdateName(updateUsr.FullName) {
			clauses["full_name"] = updateUsr.FullName
		}
		if usr.ShouldUpdateTelephone(updateUsr.Telephone) {
			clauses["telephone"] = updateUsr.Telephone
		}
		if usr.ShouldUpdateAddress(updateUsr.Address) {
			clauses["address"] = updateUsr.Address
		}
		if usr.ShouldUpdateEmail(updateUsr.Email) {
			clauses["email"] = updateUsr.Email
		}
		if usr.ShouldUpdatePassword(updateUsr.PasswordHash) {
			clauses["password_hash"] = updateUsr.PasswordHash
		}

		// Nothing to be updated
		if len(clauses) == 0 {
			return nil
		}

		res, err := sq.Update("users").
			SetMap(clauses).
			Where("id = ?", usrBinID).
			RunWith(tx).
			ExecContext(ctx)
		if err != nil {
			return fromMySQLErr(err)
		}

		affectedRows, err := res.RowsAffected()
		if err != nil {
			logrus.Error(err)
			return fromMySQLErr(err)
		}

		if affectedRows != 1 {
			err := fmt.Errorf("unexpected affected rows on user update: %d", affectedRows)
			logrus.Error(err)
			return fromMySQLErr(err)
		}

		return nil
	})
}
