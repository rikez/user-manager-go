package repository

import (
	"time"

	"gitlab.com/rikez/user-manager-go.git/internal/generator"
	"gitlab.com/rikez/user-manager-go.git/internal/model"
)

// dbUser represents an user in the system
type dbUser struct {
	ID             []byte     `db:"id"`
	FullName       *string    `db:"full_name"`
	Telephone      *string    `db:"telephone"`
	Address        *string    `db:"address"`
	Email          string     `db:"email"`
	ThirdPartyAuth *string    `db:"third_party_auth"`
	PasswordHash   *string    `db:"password_hash"`
	CreatedAt      *time.Time `db:"created_at"`
	UpdatedAt      *time.Time `db:"updated_at"`
}

// ToUser converts from database user to application user
func (dbu dbUser) ToUser() *model.User {
	return &model.User{
		ID:             generator.String(dbu.ID),
		FullName:       dbu.FullName,
		Telephone:      dbu.Telephone,
		Address:        dbu.Address,
		Email:          dbu.Email,
		ThirdPartyAuth: dbu.ThirdPartyAuth,
		PasswordHash:   dbu.PasswordHash,
		CreatedAt:      dbu.CreatedAt,
		UpdatedAt:      dbu.UpdatedAt,
	}
}

var sqlInsertColumns = []string{
	"id",
	"full_name",
	"email",
	"password_hash",
	"third_party_auth",
}

var sqlQueryColumns = []string{
	"id",
	"full_name",
	"telephone",
	"address",
	"email",
	"password_hash",
	"third_party_auth",
	"created_at",
	"updated_at",
}
