package repository

import (
	"database/sql"
	"errors"

	"github.com/go-sql-driver/mysql"
)

// MySQLDuplicateEntryErrCode represents the duplicate mysql err code
const MySQLDuplicateEntryErrCode = 1062

// IsDuplicateEntryErr verifies if the error is of duplicate type
func IsDuplicateEntryErr(err error) bool {
	var mysqlErr *mysql.MySQLError
	return errors.As(err, &mysqlErr) && mysqlErr.Number == MySQLDuplicateEntryErrCode
}

// NoRows checks if query return is empty
func NoRows(err error) bool {
	return errors.Is(err, sql.ErrNoRows)
}

func fromMySQLErr(err error) error {
	if errors.Is(err, sql.ErrNoRows) {
		return ErrNotFound
	}
	if IsDuplicateEntryErr(err) {
		return ErrAlreadyExists
	}
	return ErrDatabaseServer
}
