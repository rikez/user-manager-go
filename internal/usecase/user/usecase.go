package user

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/user-manager-go.git/internal/authorizer/oauth2provider"
	"gitlab.com/rikez/user-manager-go.git/internal/customerrors"
	"gitlab.com/rikez/user-manager-go.git/internal/generator"
	"gitlab.com/rikez/user-manager-go.git/internal/model"
	"gitlab.com/rikez/user-manager-go.git/internal/notification"
	"gitlab.com/rikez/user-manager-go.git/internal/repository"
	"gitlab.com/rikez/user-manager-go.git/pkg/restgoogle"
	"golang.org/x/crypto/bcrypt"
)

//go:generate mockery -case underscore -output mocks -outpkg userusecasesmock -name Usecases

// Usecases defines the actions the user can perform
type Usecases interface {
	// Register registers a new user into the system
	Register(context.Context, model.User) (*model.User, error)
	// RegisterFromGoogle registers a new user into the system based on google's account
	RegisterFromGoogle(ctx context.Context, token string) (*model.User, error)
	// SignIn signs the user in if credentials match
	SignIn(ctx context.Context, email, passwd string) (*model.User, error)
	// ResetPassword resets user's password
	ResetPassword(ctx context.Context, email string) error
	// VerifyResetPasswordToken verifies reset password token
	VerifyResetPasswordToken(ctx context.Context, token string) (*model.User, error)
	// GetByID returns user by ID
	GetByID(ctx context.Context, id string) (*model.User, error)
	// GetByEmail returns user by ID
	GetByEmail(ctx context.Context, email string) (*model.User, error)
	// Get returns user by ID
	Update(context.Context, model.User) (*model.User, error)
}

type defaultService struct {
	repo                    repository.Repository
	googleAPIClient         restgoogle.Client
	mailerService           notification.MailerService
	resetPasswdJWTGenerator generator.JWTGenerator
	webAppDomain            string
}

// NewService creates a new instance of User Usecases
func NewService(
	repo repository.Repository,
	googleAPIClient restgoogle.Client,
	mailerService notification.MailerService,
	jwtGenerator generator.JWTGenerator,
	webAppDomain string,
) Usecases {
	return &defaultService{
		repo:                    repo,
		googleAPIClient:         googleAPIClient,
		mailerService:           mailerService,
		resetPasswdJWTGenerator: jwtGenerator,
		webAppDomain:            webAppDomain,
	}
}

func (s *defaultService) Register(ctx context.Context, usr model.User) (*model.User, error) {
	hashBytes, err := bcrypt.GenerateFromPassword([]byte(usr.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, fmt.Errorf("failed to hash and salt password: %v", err)
	}

	passHash := string(hashBytes)

	usr.ID = generator.NewUUID().String()
	usr.PasswordHash = &passHash

	if err := s.repo.SaveUser(ctx, usr); err != nil {
		return nil, customerrors.FromDatabaseErr(err)
	}

	return s.GetByEmail(ctx, usr.Email)
}

func (s *defaultService) RegisterFromGoogle(ctx context.Context, token string) (*model.User, error) {
	userInfo, err := s.googleAPIClient.GetUserInfo(ctx, token)
	if err != nil {
		logrus.Warnf("Failed to obtain user's google account information: %s", err)
		return nil, customerrors.NewErrInternal(err)
	}

	fullName := fmt.Sprintf("%s %s", userInfo.GivenName, userInfo.FamilyName)
	authorizerType := oauth2provider.GoogleAuthorizerType.String()
	newUser := model.User{
		ID:             generator.NewUUID().String(),
		FullName:       &fullName,
		Email:          userInfo.Email,
		ThirdPartyAuth: &authorizerType,
	}
	if err := s.repo.SaveUser(ctx, newUser); err != nil {
		if errors.Is(err, repository.ErrAlreadyExists) {
			return s.GetByEmail(ctx, newUser.Email)
		}
		return nil, customerrors.FromDatabaseErr(err)
	}

	return &newUser, nil
}

func (s *defaultService) ResetPassword(ctx context.Context, email string) error {
	usr, err := s.GetByEmail(ctx, email)
	if err != nil {
		return err
	}
	if usr.ThirdPartyAuth != nil {
		return customerrors.NewErrConflict(errors.New("can't reset password because user signed in with custom authorizer"))
	}

	tokenPayload := map[string]interface{}{
		"exp":   time.Now().Add(time.Minute * 15).Unix(),
		"email": usr.Email,
		"id":    usr.ID,
	}
	token, err := s.resetPasswdJWTGenerator.Generate(tokenPayload)
	if err != nil {
		return customerrors.NewErrInternal(err)
	}

	resetPasswordURL := fmt.Sprintf("%s/reset-password/change-password?ac=%s",
		s.webAppDomain,
		s.resetPasswdJWTGenerator.Encode(token),
	)

	go func() {
		timeoutCtx, cancel := context.WithTimeout(context.Background(), time.Second*20)
		defer cancel()

		msg := notification.MailerMessage{
			Subject:   "Password Reset",
			Body:      fmt.Sprintf("Link to reset your password: %s", resetPasswordURL),
			Recipient: usr.Email,
		}
		if err := s.mailerService.Notify(timeoutCtx, msg); err != nil {
			logrus.Errorf("Failed to notify %q about password reset: %v", usr.Email, err)
			return
		}
	}()

	return nil
}

func (s *defaultService) VerifyResetPasswordToken(ctx context.Context, token string) (*model.User, error) {
	decoded, err := s.resetPasswdJWTGenerator.Decode(token)
	if err != nil {
		return nil, customerrors.NewErrInternal(err)
	}

	verified, err := s.resetPasswdJWTGenerator.Verify(decoded)
	if err != nil {
		return nil, customerrors.NewErrForbidden(err)
	}

	claims, ok := s.resetPasswdJWTGenerator.IsValid(verified)
	if !ok {
		return nil, customerrors.NewErrForbidden(errors.New("token invalid"))
	}

	userID, ok := claims["id"].(string)
	if !ok {
		return nil, customerrors.NewErrForbidden(errors.New("token payload is invalid"))
	}

	return s.GetByID(ctx, userID)
}

func (s *defaultService) SignIn(ctx context.Context, email, passwd string) (*model.User, error) {
	usr, err := s.GetByEmail(ctx, email)
	if err != nil {
		return nil, customerrors.FromDatabaseErr(err)
	}

	if usr.ThirdPartyAuth != nil {
		return nil, customerrors.NewErrForbidden(errors.New("user signed in with third party authorizer"))
	}
	if err := bcrypt.CompareHashAndPassword([]byte(*usr.PasswordHash), []byte(passwd)); err != nil {
		return nil, customerrors.NewErrForbidden(errors.New("invalid credentials"))
	}

	return usr, nil
}

func (s *defaultService) GetByID(ctx context.Context, id string) (*model.User, error) {
	usr, err := s.repo.FindUserByID(ctx, id)
	if err != nil {
		return nil, customerrors.FromDatabaseErr(err)
	}

	return usr, nil
}

func (s *defaultService) GetByEmail(ctx context.Context, email string) (*model.User, error) {
	usr, err := s.repo.FindUserByEmail(ctx, email)
	if err != nil {
		return nil, customerrors.FromDatabaseErr(err)
	}

	return usr, nil
}

func (s *defaultService) Update(ctx context.Context, usr model.User) (*model.User, error) {
	if usr.Password != "" {
		hashBytes, err := bcrypt.GenerateFromPassword([]byte(usr.Password), bcrypt.DefaultCost)
		if err != nil {
			return nil, fmt.Errorf("failed to hash and salt password: %v", err)
		}

		hashAsString := string(hashBytes)
		usr.PasswordHash = &hashAsString
	}

	if err := s.repo.UpdateUser(ctx, usr); err != nil {
		return nil, customerrors.FromDatabaseErr(err)
	}

	return s.GetByID(ctx, usr.ID)
}
