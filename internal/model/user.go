package model

import (
	"time"
)

// User represents an user in the system
type User struct {
	ID             string     `json:"id"`
	FullName       *string    `json:"full_name" validate:"omitempty,min=3"`
	Telephone      *string    `json:"telephone" validate:"omitempty,min=5"`
	Address        *string    `json:"address" validate:"omitempty,min=5"`
	Email          string     `json:"email" validate:"omitempty,email"`
	Password       string     `json:"password,omitempty" validate:"omitempty,min=8,max=50"`
	PasswordHash   *string    `json:"-"`
	ThirdPartyAuth *string    `json:"third_party_auth"`
	CreatedAt      *time.Time `json:"created_at"`
	UpdatedAt      *time.Time `json:"updated_at"`
}

// ShouldUpdateName checks if field should be updated
func (u User) ShouldUpdateName(other *string) bool {
	return shouldUpdate(u.FullName, other)
}

// ShouldUpdateTelephone checks if field should be updated
func (u User) ShouldUpdateTelephone(other *string) bool {
	return shouldUpdate(u.Telephone, other)
}

// ShouldUpdateAddress checks if field should be updated
func (u User) ShouldUpdateAddress(other *string) bool {
	return shouldUpdate(u.Address, other)
}

// ShouldUpdateEmail checks if field should be updated
func (u User) ShouldUpdateEmail(other string) bool {
	return u.ThirdPartyAuth == nil && other != "" && u.Email != other
}

// ShouldUpdatePassword checks if field should be updated
func (u User) ShouldUpdatePassword(passwdHash *string) bool {
	return shouldUpdate(u.PasswordHash, passwdHash)
}

func shouldUpdate(a, b *string) bool {
	if b == nil || *b == "" {
		return false
	}
	if a == nil {
		return true
	}
	if a != nil && *a != *b {
		return true
	}
	return false
}
