package notification

import (
	"context"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/sirupsen/logrus"
)

// SendgridMailerConfig defines the configuration params for the mailer
type SendgridMailerConfig struct {
	APIKey string `required:"true"`
}

type sendgridNotification struct {
	sg *sendgrid.Client
}

// NewSendgridNotification creates a new instance Mailgun mailer
func NewSendgridNotification(cfg SendgridMailerConfig) MailerService {
	return &sendgridNotification{
		sg: sendgrid.NewSendClient(cfg.APIKey),
	}
}

func (s *sendgridNotification) Notify(ctx context.Context, msg MailerMessage) error {
	from := mail.NewEmail("Enrico", sender)
	to := mail.NewEmail("User", msg.Recipient)
	message := mail.NewSingleEmail(from, msg.Subject, to, msg.Body, msg.Body)
	response, err := s.sg.Send(message)
	if err != nil {
		return err
	}

	logrus.Info(response)

	return nil
}
