package webapp

import (
	"encoding/gob"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/rikez/user-manager-go.git/internal/authorizer/oauth2provider"
	"gitlab.com/rikez/user-manager-go.git/pkg/restusermanager"
)

const (
	cookieOAuthStateName = "oauth_validation_state"
	cookieAuthSession    = "ck_auth_sess"
)

var tpl *template.Template

func init() {
	gob.Register(userSession{})
	tpl = template.Must(template.ParseFiles(
		"web/index.html",
		"web/reset-password.html",
		"web/reset-password-email-sent.html",
		"web/reset-password-change-password.html",
		"web/signin.html",
		"web/signup.html",
		"web/user/profile.html",
		"web/user/edit-profile.html",
	))
}

// Server acts as a web server resposible for rendering, auth, etc
type Server struct {
	googleAuthorizer  oauth2provider.Authorizer
	userManagerClient restusermanager.Client
	sessionSecret     string
	sessionsStore     *sessions.CookieStore
}

// NewServer creates anew instance of server
func NewServer(googleAuthorizer oauth2provider.Authorizer,
	userManagerClient restusermanager.Client,
	sessionSecret, domain string) *Server {
	store := sessions.NewCookieStore([]byte(sessionSecret))
	store.Options = &sessions.Options{
		MaxAge:   3600,
		HttpOnly: true,
		Domain:   domain,
		Path:     "/",
	}
	return &Server{
		googleAuthorizer:  googleAuthorizer,
		userManagerClient: userManagerClient,
		sessionSecret:     sessionSecret,
		sessionsStore:     store,
	}
}

// AddRoutes defines routers' handlers
func (s *Server) AddRoutes(r *mux.Router) {
	r.HandleFunc("/", s.wrappedHandler(s.loginPage))
	r.HandleFunc("/logout", s.wrappedHandler(s.logout))
	r.HandleFunc("/auth/google", s.wrappedHandler(s.signInGoogle))
	r.HandleFunc("/auth/google/callback", s.wrappedHandler(s.signInGoogleCallback))
	r.HandleFunc("/user/profile", s.wrappedHandler(s.userProfilePage))
	r.HandleFunc("/user/profile/edit", s.wrappedHandler(s.userProfileEditPage))
	r.HandleFunc("/reset-password", s.wrappedHandler(s.resetPasswordPage))
	r.HandleFunc("/reset-password/email-sent", s.wrappedHandler(s.resetPasswordSuccessPage))
	r.HandleFunc("/reset-password/change-password", s.wrappedHandler(s.changePasswordPage))
	r.HandleFunc("/signin", s.wrappedHandler(s.signinPage))
	r.HandleFunc("/signup", s.wrappedHandler(s.signupPage))

	// Form submissions
	r.HandleFunc("/users/create", s.wrappedHandler(s.createUser))
	r.HandleFunc("/users/update", s.wrappedHandler(s.updateUser))
	r.HandleFunc("/users/signin", s.wrappedHandler(s.signInUser))
	// request change
	r.HandleFunc("/users/reset_password", s.wrappedHandler(s.resetPassword))
	// change the pass
	r.HandleFunc("/users/change_password", s.wrappedHandler(s.changePassword))
}

func (s *Server) loginPage(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusTemporaryRedirect)
		return
	}

	if err := tpl.ExecuteTemplate(w, "index.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) logout(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = userSession{}
	session.Options.MaxAge = -1

	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/", http.StatusPermanentRedirect)
}

func (s *Server) signInGoogle(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusOK)
		return
	}

	// Getting redirect config
	redirectSignIn := s.googleAuthorizer.GetRedirectConfig()
	state := redirectSignIn.State.Encode()

	// Generating cookie for the oauth2
	cookie := generateCookie(cookieOAuthStateName, state, s.sessionsStore.Options.Domain)
	http.SetCookie(w, cookie)

	// Getting redirect URL with state
	redirectURL := redirectSignIn.Config.AuthCodeURL(state)

	http.Redirect(w, r, redirectURL, http.StatusTemporaryRedirect)
}

func (s *Server) signInGoogleCallback(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	// Validate cookie to ensure it is coming from the expected form
	oauth2StateCookie, err := r.Cookie(cookieOAuthStateName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
	}

	// Validate state integrity, otherwise return user to login page
	if r.FormValue("state") != oauth2StateCookie.Value {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	if code == "" {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	exchange, err := s.googleAuthorizer.Exchange(ctx, code)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusInternalServerError)
		return
	}

	registeredUsr, err := s.userManagerClient.RegisterFromGoogle(ctx, restusermanager.RegisterGoogleUserRequest{
		Token: exchange.AccessToken,
	})
	if err != nil {
		http.Redirect(w, r, "/error", http.StatusInternalServerError)
		return
	}

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = &userSession{
		ID:            registeredUsr.ID,
		Email:         registeredUsr.Email,
		GoogleJWT:     exchange.AccessToken,
		Authenticated: true,
	}

	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user/profile", http.StatusFound)
}

func (s *Server) userProfilePage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if !usr.Authenticated {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	foundUser, err := s.userManagerClient.GetUserByID(ctx, usr.ID)
	if err != nil {
		// TODO: improve error handling
		if err.Error() == "not found" {
			http.Redirect(w, r, "/logout", http.StatusTemporaryRedirect)
			return
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tpl.ExecuteTemplate(w, "profile.html", foundUser); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) userProfileEditPage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if !usr.Authenticated {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	foundUser, err := s.userManagerClient.GetUserByID(ctx, usr.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tpl.ExecuteTemplate(w, "edit-profile.html", foundUser); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) resetPasswordPage(w http.ResponseWriter, r *http.Request) {
	if err := tpl.ExecuteTemplate(w, "reset-password.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) resetPasswordSuccessPage(w http.ResponseWriter, r *http.Request) {
	if err := tpl.ExecuteTemplate(w, "reset-password-email-sent.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) changePasswordPage(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usrSess := getUserSession(session)
	if usrSess.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
		return
	}

	ctx := r.Context()

	token := r.FormValue("ac")
	if token == "" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}

	usr, err := s.userManagerClient.VerifyResetPasswordToken(ctx, restusermanager.VerifyResetPasswordTokenRequest{
		Token: token,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = &userSession{
		Authenticated: false,
		ID:            usr.ID,
		Email:         usr.Email,
	}
	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tpl.ExecuteTemplate(w, "reset-password-change-password.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) changePassword(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usrSess := getUserSession(session)
	if usrSess.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
		return
	}
	if usrSess.ID == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	ctx := r.Context()

	passwd := r.FormValue("password")
	if passwd == "" {
		http.Error(w, "Empty password", http.StatusBadRequest)
		return
	}

	_, err = s.userManagerClient.UpdateUser(ctx, usrSess.ID, restusermanager.UpdateUserRequest{
		Password: passwd,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/signin", http.StatusFound)
}

func (s *Server) signupPage(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
		return
	}

	if err := tpl.ExecuteTemplate(w, "signup.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) createUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
		return
	}

	email := r.FormValue("email")
	passwd := r.FormValue("password")
	if email == "" || passwd == "" {
		http.Error(w, "Invalid credentials", http.StatusInternalServerError)
		return
	}

	payload := restusermanager.RegisterUserRequest{
		Email:    email,
		Password: passwd,
	}
	registeredUsr, err := s.userManagerClient.Register(ctx, payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = &userSession{
		ID:            registeredUsr.ID,
		Email:         registeredUsr.Email,
		GoogleJWT:     "",
		Authenticated: true,
	}
	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user/profile", http.StatusFound)
}

func (s *Server) updateUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if !usr.Authenticated {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	payload := restusermanager.UpdateUserRequest{
		Email:     r.FormValue("email"),
		FullName:  r.FormValue("full_name"),
		Telephone: r.FormValue("telephone"),
		Address:   r.FormValue("address"),
	}
	registeredUsr, err := s.userManagerClient.UpdateUser(ctx, usr.ID, payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = &userSession{
		ID:            registeredUsr.ID,
		Email:         registeredUsr.Email,
		GoogleJWT:     "",
		Authenticated: true,
	}
	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user/profile", http.StatusFound)
}

func (s *Server) signInUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
		return
	}

	payload := restusermanager.SignInRequest{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}
	signedInUsr, err := s.userManagerClient.SignIn(ctx, payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = &userSession{
		ID:            signedInUsr.ID,
		Email:         signedInUsr.Email,
		GoogleJWT:     "",
		Authenticated: true,
	}
	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user/profile", http.StatusFound)
}

func (s *Server) resetPassword(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
		return
	}

	payload := restusermanager.ResetPasswordRequest{
		Email: r.FormValue("email"),
	}
	if err = s.userManagerClient.ResetPassword(ctx, payload); err != nil {
		// TODO: improve err handling
		if err.Error() == "not found" {
			http.Error(w, "Email doesn't exist", http.StatusInternalServerError)
			return
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/reset-password/email-sent", http.StatusFound)
}

func (s *Server) signinPage(w http.ResponseWriter, r *http.Request) {
	session, err := s.sessionsStore.Get(r, cookieAuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	usr := getUserSession(session)
	if usr.Authenticated {
		http.Redirect(w, r, "/user/profile", http.StatusOK)
		return
	}

	if err := tpl.ExecuteTemplate(w, "signin.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
