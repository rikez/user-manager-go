package webapp

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/user-manager-go.git/internal/generator"
)

var defaultCookieExpiration = time.Now().Add(365 * 24 * time.Hour)

func generateCookie(name, value, domain string) *http.Cookie {
	return &http.Cookie{
		Name:    name,
		Value:   value,
		Expires: defaultCookieExpiration,
		Domain:  domain,
		Path:    "/",
	}
}

func (s *Server) wrappedHandler(h func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	var handler http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
		t0 := time.Now()
		reqID := generator.NewUUID().String()
		fields := logrus.Fields{"reqid": reqID}
		logrus.WithFields(fields).Infof("Request: %s %s", r.Method, r.URL.Path)

		// TODO: find a way to get status code
		h(w, r)
		elapsedMs := float64(time.Now().UnixNano()-t0.UnixNano()) / float64(time.Millisecond)

		logrus.WithFields(fields).Infof("Response: %s %s -> (%.2f ms)", r.Method, r.URL.Path, elapsedMs)
	}

	return handler
}
