package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/user-manager-go.git/internal/generator"
	"gitlab.com/rikez/user-manager-go.git/internal/notification"
	"gitlab.com/rikez/user-manager-go.git/internal/repository"
	"gitlab.com/rikez/user-manager-go.git/internal/rest"
	"gitlab.com/rikez/user-manager-go.git/internal/usecase/user"
	"gitlab.com/rikez/user-manager-go.git/pkg/httpserver"
	"gitlab.com/rikez/user-manager-go.git/pkg/mysql"
	"gitlab.com/rikez/user-manager-go.git/pkg/restgoogle"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(os.Stdout)
}

func main() {
	logrus.Info("Initializing rest service...")

	cfg, err := getConfig()
	if err != nil {
		logrus.Panicf("Failed to get env config: %s", err)
	}

	timeoutCtx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()
	db, err := mysql.Connect(timeoutCtx, &cfg.MySQL)
	if err != nil {
		logrus.Panic(err)
	}

	dbx := sqlx.NewDb(db, "mysql")
	mysqlRepository := repository.NewMySQLRepository(dbx)

	resetPasswdTokenGenerator := generator.NewJWTGenerator(cfg.ResetPasswdToken)
	sendgridService := notification.NewSendgridNotification(cfg.SendgridMailer)

	// TODO: improve http client creation...add transport config, trippers, etc
	httpClient := http.Client{Timeout: time.Second * 5}
	googleAPIClient := restgoogle.NewClient(cfg.GoogleAPI, &httpClient)
	userService := user.NewService(mysqlRepository,
		googleAPIClient,
		sendgridService,
		resetPasswdTokenGenerator,
		cfg.WebAPPDomain,
	)

	// Rest
	logrus.Info("Starting rest HTTP Server...")
	restRouter := mux.NewRouter()
	restSrv := rest.NewServer(userService)
	restSrv.AddRoutes(restRouter)
	restHTTPServer := httpserver.Start(restRouter, cfg.Rest.HTTPServerPort)

	logrus.Info("Rest service is running")

	// TODO: improve by creating a interface for the Closer tasks...
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)
	<-exit

	logrus.Info("Terminating...")
	timeoutCtx, cancel = context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	restHTTPServer.Stop(timeoutCtx)
	db.Close()

	logrus.Info("Terminated")
}
