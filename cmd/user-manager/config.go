package main

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/rikez/user-manager-go.git/internal/generator"
	"gitlab.com/rikez/user-manager-go.git/internal/notification"
	"gitlab.com/rikez/user-manager-go.git/internal/rest"
	"gitlab.com/rikez/user-manager-go.git/pkg/mysql"
	"gitlab.com/rikez/user-manager-go.git/pkg/restgoogle"
)

type config struct {
	GoogleAPI restgoogle.Config

	SendgridMailer notification.SendgridMailerConfig

	ResetPasswdToken generator.JWTGeneratorConfig

	WebAPPDomain string `required:"true"`

	Rest rest.Config

	MySQL mysql.Config
}

func getConfig() (*config, error) {
	cfg := config{}

	if err := envconfig.Process("usermanager", &cfg); err != nil {
		return nil, fmt.Errorf("failed to read the env: %v", err)
	}
	return &cfg, nil
}
