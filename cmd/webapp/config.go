package main

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/rikez/user-manager-go.git/internal/authorizer/oauth2provider"
	"gitlab.com/rikez/user-manager-go.git/internal/webapp"
	"gitlab.com/rikez/user-manager-go.git/pkg/restusermanager"
)

type config struct {
	GoogleOAuth2 oauth2provider.Config
	UserManager  restusermanager.Config

	CookieDomain string `required:"true"`

	WebAPP webapp.Config
}

func getConfig() (*config, error) {
	cfg := config{}

	if err := envconfig.Process("webapp", &cfg); err != nil {
		return nil, fmt.Errorf("failed to read the env: %v", err)
	}
	return &cfg, nil
}
