# User Manager

An experiement with the Google Identity APIs to allow users to Sign in/Sign up and manage their profile. The users can choose to use Gmail or create an account from scratch.

## Failure Case

* When MySQL is outage
* When Google OAuth API is outage

## Decisions

* Use MySQL for persistence
* Expose Rest service only internally

## Architecture

```mermaid
graph LR
subgraph Internal
    N(nginx) --> |Proxy HTTP Requests| WA(webapp)
    WA --> |HTTP Rest| UM(user-manager)
    UM --> MYSQL(mysql)
end
subgraph Client
   C(Client) --> N
end
subgraph 3rd Party
   UM --> |Redirect|GA(google-auth2)
   GA --> |Callback|UM
   UM --> |HTTP Rest|SG(sendgrid)
   UM --> |HTTP Rest|GU(google-user-api)
end
```

## References

* [Google OAuth 2.0 API](https://developers.google.com/identity/protocols/oauth2)
* [Go HTML Templates](https://golang.org/pkg/html/template/)

## Improvements

* Rewrite UI and Go templates: use template partials, separate CSS and JS files, etc
* Improve error handling
* Revisit the environment variables for possible renames
* Write acceptance and unit tests
* Add microservice protection mechanisms like circuit breakers
* Move session to a cache storage, like redis or memcached
* Improve local execution scripts
* Add a CI to bitbucket-pipelines
* Add documentation
* Improve dockerfiles (create CI images, apply multi-stage build, etc)



