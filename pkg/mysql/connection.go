package mysql

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"
	"net"
	"time"

	// This is needed in order to initialize the MySQL driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
)

// Connect returns the connection of the MySQL DB indicated by the configuration.
func Connect(ctx context.Context, cfg *Config) (*sql.DB, error) {
	for {
		db, err := connect(cfg)
		if err == nil || !isNetworkError(err) {
			return db, err
		}

		logrus.Infof("Retrying mysql connection: %v", err)

		select {
		case <-time.After(cfg.ConnRetrySleep):
		case <-ctx.Done():
			return nil, ctx.Err()
		}
	}
}

func connect(cfg *Config) (*sql.DB, error) {
	if cfg.Connection == "" {
		return nil, errors.New("connection value is unset")
	}

	// Add default options to the connection string
	conn, err := connWithDefaultOpts(cfg.Connection)
	if err != nil {
		return nil, err
	}

	// Open the connection and write the settings
	client, err := sql.Open("mysql", conn)
	if err != nil {
		return nil, err
	}
	client.SetConnMaxLifetime(cfg.ConnMaxLifetime)
	client.SetMaxIdleConns(int(cfg.MaxIdleConns))
	client.SetMaxOpenConns(int(cfg.MaxOpenConns))

	// We ping just to ensure the connection is established
	if err := client.Ping(); err != nil {
		if err := client.Close(); err != nil {
			return nil, fmt.Errorf("close MySQL connection after ping failure: %v", err)
		}
		return nil, err
	}

	return client, nil
}

func isNetworkError(err error) bool {
	if _, ok := err.(net.Error); ok {
		return true
	}
	if err == driver.ErrBadConn {
		return true
	}
	return false
}
