package restusermanager

import (
	"fmt"
	"net/url"
)

// Config defines specific config for the restgoogle package
type Config struct {
	Address urlDecoder `default:"http://localhost:8081"`
}

// See https://github.com/kelseyhightower/envconfig#custom-decoders
type urlDecoder url.URL

func (c *urlDecoder) Decode(value string) error {
	parsedURL, err := url.Parse(value)
	if err != nil {
		return fmt.Errorf("invalid connection value in '%s'", value)
	}
	*c = urlDecoder(*parsedURL)
	return c.validate()
}

func (c *urlDecoder) validate() error {
	if c.Scheme != "http" && c.Scheme != "https" {
		return fmt.Errorf("invalid scheme value '%s' in connection", c.Scheme)
	}
	return nil
}
