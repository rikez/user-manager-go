package restgoogle

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnectionDecoder_Decode(t *testing.T) {
	cases := []struct {
		name    string
		address string
		// TODO: improve by impl custom error types
		errored bool
	}{
		{
			name:    "valid HTTP conn",
			address: "http://hostname:8080",
			errored: false,
		},
		{
			name:    "valid HTTPS conn",
			address: "https://hostname:8080",
			errored: false,
		},
		{
			name:    "invalid conn",
			address: "Unknown connection",
			errored: true,
		},
		{
			name:    "invalid scheme",
			address: "ftp://hostname:8080",
			errored: true,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			var conn urlDecoder
			err := conn.Decode(tt.address)
			// See `errored` definition
			if tt.errored {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
