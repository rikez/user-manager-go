package restgoogle

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/require"
	"gitlab.com/rikez/user-manager-go.git/test/testutil"
)

func TestClient_GetApplicantByID(t *testing.T) {
	var method, path, accessToken string
	fakeAccessToken := "fake-token123"
	httpClient := http.Client{}

	require := require.New(t)

	t.Run("returns success response when status is 200", func(t *testing.T) {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			method = r.Method
			path = r.URL.Path
			accessToken = r.URL.Query().Get("access_token")

			f := testutil.ReadTestData(t, "get_user_info.golden.json")
			defer f.Close()

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_, _ = io.Copy(w, f)
		}))
		defer ts.Close()

		newURL, err := url.Parse(ts.URL)
		require.NoError(err)

		c := NewClient(Config{Address: urlDecoder(*newURL)}, &httpClient)
		require.NotNil(t, c)

		actual, err := c.GetUserInfo(context.Background(), fakeAccessToken)
		require.NoError(err)
		require.NotNil(actual)

		require.Equal(http.MethodGet, method)
		require.Equal(getUserInfoPath, path)
		require.Equal(fakeAccessToken, accessToken)

		expected := &GetUserInfoResponse{}
		testutil.DecodeTestData(t, testutil.ReadTestData(t, "get_user_info.golden.json"), expected)

		require.Nil(deep.Equal(expected, actual))
	})

	t.Run("returns error when status is not 200", func(t *testing.T) {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			method = r.Method
			path = r.URL.Path
			accessToken = r.URL.Query().Get("access_token")

			f := testutil.ReadTestData(t, "get_user_info_invalid.golden.json")
			defer f.Close()

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			_, _ = io.Copy(w, f)
		}))
		defer ts.Close()

		newURL, err := url.Parse(ts.URL)
		require.NoError(err)

		c := NewClient(Config{Address: urlDecoder(*newURL)}, &httpClient)
		require.NotNil(t, c)

		actual, err := c.GetUserInfo(context.Background(), fakeAccessToken)
		require.Error(err)
		require.Nil(actual)

		require.Equal(http.MethodGet, method)
		require.Equal(getUserInfoPath, path)
		require.Equal(fakeAccessToken, accessToken)

		expected := &ErrorResponse{}
		testutil.DecodeTestData(t, testutil.ReadTestData(t, "get_user_info_invalid.golden.json"), expected)

		require.Nil(deep.Equal(expected, err))
	})
}
