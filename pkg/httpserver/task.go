package httpserver

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/sirupsen/logrus"
)

type HTTPServer struct {
	srv *http.Server
}

func Start(handler http.Handler, port uint32) *HTTPServer {
	cors := handlers.CORS(
		handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization", "Cache-Control", "Expires"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	cacheHandler := func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Cache-Control", "no-cache, no-store")
			w.Header().Set("Expires", time.Unix(0, 0).Format(http.TimeFormat))
			h.ServeHTTP(w, r)
		})
	}

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: cors(cacheHandler(handler)),
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				logrus.Panicf("Failed to serve a HTTP Server: %v", err)
			}
		}
	}()

	return &HTTPServer{srv: srv}
}

func (s *HTTPServer) Stop(ctx context.Context) {
	if err := s.srv.Shutdown(ctx); err != nil {
		logrus.Panic(err)
	}
}
