#!/usr/bin/env bash

if [[ $# -ne 1 ]]; then
    echo "You should provide something to build as first argument"
    exit 1
fi

echo "Building localdb $1"
docker build localdb/"$1" -t "enrich/$1:latest" --build-arg BUILD_TAG="latest"