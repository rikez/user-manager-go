#!/bin/bash

# Compile this service

# Check required commands
command -v go >/dev/null 2>&1 || { echo 'please install go or use image that has it'; exit 1; }

paths=$(find "cmd" -maxdepth 1 -mindepth 1 -name "*")

if [[ $(echo "$paths" | wc -l) -eq 0 ]]; then
  echo "No commands found"
  exit 1
fi

for path in $paths; do
    cmd=$(basename "$path")
    cmd_path="$PWD/cmd/$cmd"
    bin_path="$PWD/dockerfiles/$cmd/bin/$cmd"
    
    echo "Compiling $cmd"
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o "$bin_path" "$cmd_path"
    echo "Done"
done


mkdir -p $PWD/dockerfiles/webapp/web
cp -r $PWD/web/* $PWD/dockerfiles/webapp/web