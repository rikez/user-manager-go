#!/bin/bash

# Install external tools

# Check required commands
command -v go > /dev/null 2>&1 || { echo 'please install go or use image that has it'; exit 1; }

GO111MODULE=off go get -u -v \
		golang.org/x/tools/cmd/goimports \
		golang.org/x/lint/golint \
		honnef.co/go/tools/cmd/staticcheck \
		github.com/kisielk/errcheck \
		go get github.com/golangci/golangci-lint/cmd/golangci-lint \
		github.com/kyoh86/richgo \
		github.com/vektra/mockery/.../